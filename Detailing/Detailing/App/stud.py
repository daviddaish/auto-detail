import math

import FreeCAD
import Part
import DraftGeomUtils
import DraftVecUtils

import Detailing.App.common as det_common
import Detailing.App.dwang as det_dwang

def make_studs (
        stud_space, flush_face, cardinal_pnts, 
        stud_width=45, stud_depth=75, min_stud_spacing=400,
        sections=[]
        ):

    # Determine relevant edges, faces and cardinal locations
    parallel_face = det_common.cardinal_parallel(flush_face)
    sides = det_common.find_sides(stud_space)
    bot_cords = det_common.find_cardinal(sides["bot"], cardinal_pnts)

    # Determine depth vector
    depth_vector = bot_cords[parallel_face[0]] - bot_cords[flush_face[0]]
    depth_vector = depth_vector / depth_vector.Length
    stud_depth_vector = depth_vector * stud_depth

    # Get the top and bottom flush edges
    bot_flush_edge = Part.LineSegment(
            bot_cords[flush_face[0]],
            bot_cords[flush_face[1]]
            ).toShape().Edges[0]

    top_flush_edges = []
    for face in sides["top"]:
        card_pnts = det_common.find_cardinal(face, cardinal_pnts)
        flush_edge = Part.LineSegment(
                card_pnts[flush_face[0]], 
                card_pnts[flush_face[1]]
                ).toShape().Edges[0]
        top_flush_edges.append(flush_edge)

    # If there are no sections, just make one normal stud section
    if len(sections) == 0:
        normal_section(
                bot_flush_edge, top_flush_edges, stud_space, flush_face, sides,
                stud_depth_vector, min_stud_spacing, stud_width
                )

    else:
        sections = sorted(sections, key=lambda s: s["start"])

        previous_end_point = 0

        for section in sections:

            # Add the normal studs before the section
            normal_bot_flush_edge = Part.LineSegment(
                    bot_flush_edge.valueAt(previous_end_point),
                    bot_flush_edge.valueAt(section["start"])
                    ).toShape().Edges[0]

            normal_section(
                    normal_bot_flush_edge, top_flush_edges, stud_space, flush_face,
                    sides, stud_depth_vector, min_stud_spacing, stud_width
                    )

            previous_end_point = section["end"]

        # Add the final normal studs
        final_bot_flush_edge = Part.LineSegment(
                    bot_flush_edge.valueAt(previous_end_point),
                    bot_flush_edge.valueAt(bot_flush_edge.Length)
                    ).toShape().Edges[0]

        normal_section(
                final_bot_flush_edge, top_flush_edges, stud_space, flush_face,
                sides, stud_depth_vector, min_stud_spacing, stud_width
                )

    # # Create dwangs

    # # Dwangs shall be spaced at not more than 1350 mm centre-to-centre and
    # # shall be of not less than 45 mm x 45 mm. Dwangs may be staggered either
    # # side of a horizontal straight line by a centre-to-centre distance not
    # # exceeding 300 mm.

    # # Dwang specs
    # dwang_width = 45
    # dwang_depth = 45
    # dwang_stagger = 80

    # det_dwang.make_dwangs(stud_spaces, flush_face, dwang_width, dwang_depth,
    #         depth_vector, dwang_stagger)

def normal_section(bot_flush_edge, top_flush_edges, 
        stud_space, flush_face, sides, depth_vector,
        min_stud_spacing, stud_width
        ):

    parallel_face = det_common.cardinal_parallel(flush_face)

    # Find distance between studs
    stud_spacing = find_stud_spacing(bot_flush_edge, min_stud_spacing, stud_width)

    # Find coordinates for individual stud bases
    stud_start_point = 0
    stud_bases = []
    while True:

        stud_base = {
            flush_face[0]: bot_flush_edge.valueAt(stud_start_point),
            flush_face[1]: bot_flush_edge.valueAt(stud_start_point + stud_width)
            }

        stud_base[parallel_face[0]] = stud_base[flush_face[0]] + depth_vector
        stud_base[parallel_face[1]] = stud_base[flush_face[1]] + depth_vector

        stud_bases.append(stud_base)

        stud_start_point = stud_start_point + stud_width + stud_spacing

        if stud_start_point > (bot_flush_edge.Length + 1):
            break

    stud_spaces = []
    for index, stud_base in enumerate(stud_bases):

        # To find top points of studs, draw lines along plumb, and see where they
        # intersect with the top of stud space.
        top_point_1 = extend_stud(sides["plumb_vec"], stud_base[flush_face[0]], top_flush_edges)
        top_point_2 = extend_stud(sides["plumb_vec"], stud_base[flush_face[1]], top_flush_edges)

        if top_point_1 is None or top_point_2 is None:
            print(("Failed to create stud at index {}, because stud's extension "
                    "did not connect to the top flush edge").format(index))
            continue

        stud_top = {
            flush_face[0]: top_point_1,
            flush_face[1]: top_point_2,
            parallel_face[0]: top_point_1 + depth_vector,
            parallel_face[1]: top_point_2 + depth_vector
            }

        stud_spaces.append({
            "bot": stud_base,
            "top": stud_top
            })
    middle_point = bot_flush_edge.valueAt(bot_flush_edge.Length / 2)
    middle_point = bot_flush_edge.valueAt(bot_flush_edge.Length / 2)

    # Create studs
    for index in range(len(stud_spaces)):
        try:
            stud = det_common.make_cuboid(stud_spaces[index]["bot"], stud_spaces[index]["top"])
            stud_object = FreeCAD.ActiveDocument.addObject("Part::Feature", "stud")
            stud_object.Shape = stud
        except ValueError as ve:
            print("Failed to create stud at index {}, on account of ValueError: {}".format(index, ve)) 

# def ingress_section(bot_flush_edge, sides, cardinal):

# Finds the top face of an opening closest to a line
def find_opening_top(sides, bot_flush_edge):

    middle_point = bot_flush_edge.valueAt(bot_flush_edge.Length / 2)

    tops = [s for s in sides["inter"] if s.normalAt(0,0) == sides["plumb_vec"]]

    if len(tops) == 1:
        return tops[0]

    all_points = []
    top_indexes = []
    for index, top in enumerate(tops):
        points = [v.Point for v in top.Vertexes]
        for point in points:

            distance_to_point = (point - middle_point).Length
            height_negated_point = point + (sides["plumb_vec"] * distance_to_point)
            all_points.append(height_negated_point)
            top_indexes.append(index)

    points_index = DraftGeomUtils.findClosest(middle_point, all_points)
    top_index = top_indexes[points_index]

    return tops[top_index]

def extend_stud(plumb, base_point, top_edges):

    upwards_vec = (-plumb) * 30000
    test_line = Part.LineSegment(base_point, base_point + upwards_vec).toShape().Edges[0]

    intersection = None

    for top_edge in top_edges:
        intersections = DraftGeomUtils.findIntersection(test_line, top_edge)
        if len(intersections) == 1:
            intersection = intersections[0]
            break

    return intersection

def find_stud_spacing(travel_edge, min_stud_spacing, stud_width):

    # Determine number of studs
    stud_num = math.ceil(travel_edge.Length / min_stud_spacing) 
    spacing_num = stud_num + 1 
    space_sans_studs = travel_edge.Length - (stud_num * stud_width)
    stud_spacing = (space_sans_studs - stud_width * 2) / spacing_num

    return stud_spacing
