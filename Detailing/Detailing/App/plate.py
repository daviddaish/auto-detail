import Detailing.App.common as det_common

def make_bottom_plates(frame_space, bottom_faces, plumb, cardinal_pnts, plate_thickness):

    # Find the vector to extend the corners
    extension_vector = (-plumb) * (plate_thickness / plumb.Length)

    plates = []
    for face in bottom_faces:
        # Get the coordinates of the bottom plate cuboid
        bot_cords = det_common.find_cardinal(face, cardinal_pnts)
        extend_cords = {k: v + extension_vector for k,v in bot_cords.items()}
        plate = det_common.make_cuboid(bot_cords, extend_cords)

        # Remove any plate extending outside of the frame space
        subtracted_plate = plate.common(frame_space)
        plates.append(subtracted_plate)

    return plates

def make_top_plates(frame_space, top_faces, plumb, cardinal_pnts, plate_thickness):

    # Find the vector to extend the corners
    extension_vector = plumb * (plate_thickness / plumb.Length)

    plates = []
    for face in top_faces:
        # Get the coordinates of the top plate cuboid
        top_cords = det_common.find_cardinal(face, cardinal_pnts)
        extend_cords = {k: v + extension_vector for k,v in top_cords.items()}
        plate = det_common.make_cuboid(top_cords, extend_cords)

        # Removed any plate extending outside of the frame space
        subtracted_plate = plate.common(frame_space)
        plates.append(subtracted_plate)

    return plates
