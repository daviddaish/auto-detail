import FreeCAD
if FreeCAD.GuiUp:
    import FreeCADGui as Gui
import Part
import DraftVecUtils
import DraftGeomUtils

import Detailing.App.common as det_common
import Detailing.App.plate as det_plate
import Detailing.App.stud as det_stud

def make_frame(sel_object):

    # Hidden assumptions and requirements:
    # * Assumes base face of wall is the one with the lowest z positions.
    # * Assumes the normal of the bottom face is also plumb
    # * Assumes all sides are at right angles with the base, ie: are plumb.
    # * Requires all edges and faces to not be curved.
    # * Assumes all top faces are rectangles. This may not be the case if the
    #   top plate needs to have mitre cuts, but I cannot think of any cases for
    #   this yet.

    FreeCAD.ActiveDocument.openTransaction("Create frame")

    shape = sel_object.Shape

    # TODO: Add check for curves in edges

    # Get the the base
    bases = det_common.find_bases(shape)
    total_base_cardinal = det_common.find_cardinal(bases)

    # Determine length dimension and depth dimension
    dis_ne = total_base_cardinal["n"].distanceToPoint(total_base_cardinal["e"])
    dis_nw = total_base_cardinal["n"].distanceToPoint(total_base_cardinal["w"])
    if dis_ne > dis_nw:
        leng_cord = ("n", "e")
        dep_cord = ("n", "w")
    else:
        leng_cord = ("n", "w")
        dep_cord = ("n", "e")

    # Define the flush face
    flush_face = leng_cord

    # Find the side and top faces
    sides = det_common.find_sides(shape)

    # Determine which side faces are along the length direction
    leng_vector = total_base_cardinal[leng_cord[1]] - total_base_cardinal[leng_cord[0]]
    length_faces = []
    for face in sides["side"]:
        if det_common.is_right_angle(
                DraftVecUtils.angle(face.normalAt(0,0), leng_vector)
                ):
            length_faces.append(face)

    if len(length_faces) > 2:
        raise ValueError(("There are more than two faces that are along "
                          "the length direction of the shape."))
    if len(length_faces) < 2:
        raise ValueError(("There are less than than two faces that are along "
                          "the length direction of the shape."))

    # Define top and bottom plate specs
    plate_depth = 90
    plate_thickness = 75

    # Determine how deep to have the frame into the wall
    prior_space_distribution = 0.5
    shape_depth_edge = Part.LineSegment(
            total_base_cardinal[dep_cord[0]],
            total_base_cardinal[dep_cord[1]]
            ).toShape().Edges[0]
    remainder = shape_depth_edge.Length - plate_depth 
    if remainder < 0:
        raise ValueError(("The selected shape is too shallow "
                          "to fit the frame inside."))
    prior_remainder = remainder * prior_space_distribution

    # Shrink the depth of the wall, by moving the length faces inwards to the
    # center of the wall shape
    frame_space = shape
    for face in length_faces:
        normal = face.normalAt(0,0)
        movement = prior_remainder * (-normal)
        non_frame_space = face.extrude(movement)
        if prior_remainder > 0:
            frame_space = frame_space.cut(non_frame_space)

    # Determine the lengths of regular, window, and ingress walls.
    frame_bases = det_common.find_bases(frame_space)
    frame_base_cardinal = det_common.find_cardinal(
            frame_bases, orient_cords=total_base_cardinal)
    flush_edge = Part.LineSegment(
            frame_base_cardinal[leng_cord[0]],
            frame_base_cardinal[leng_cord[1]]
            ).toShape().Edges[0]

    frame_sides = det_common.find_sides(frame_space)
    
    frame_internal_groups = det_common.group_by_contact(frame_sides["inter"])
    ingresses = []
    windows = []

    for group in frame_internal_groups:
        opening_type = det_common.type_of_opening(group)
        if opening_type == "ingress":
            ingresses.append(group)
        elif opening_type == "window":
            windows.append(group)
        else:
            raise ValueError("This opening is not a window or an ingress.")

    flush_edge_start = flush_edge.valueAt(0)

    sections = []
    for ingress in ingresses:

        highest_face = det_common.find_highest(ingress)
        face_card = det_common.find_cardinal(
                highest_face, 
                orient_cords=total_base_cardinal
                )

        vec_1 = DraftGeomUtils.findDistance(face_card[leng_cord[0]], flush_edge)
        point_1 = face_card[leng_cord[0]] + vec_1
        diff_1 = flush_edge_start.distanceToPoint(point_1)
        vec_2 = DraftGeomUtils.findDistance(face_card[leng_cord[1]], flush_edge)
        point_2 = face_card[leng_cord[1]] + vec_2
        diff_2 = flush_edge_start.distanceToPoint(point_2)
        
        sections.append({
            "start": diff_1,
            "end": diff_2,
            "type": "ingress"
            })

    for window in windows:

        highest_face = det_common.find_highest(window)
        face_card = det_common.find_cardinal(
                highest_face,
                orient_cords=total_base_cardinal
                )

        vec_1 = DraftGeomUtils.findDistance(face_card[leng_cord[0]], flush_edge)
        point_1 = face_card[leng_cord[0]] + vec_1
        diff_1 = flush_edge_start.distanceToPoint(point_1)
        vec_2 = DraftGeomUtils.findDistance(face_card[leng_cord[1]], flush_edge)
        point_2 = face_card[leng_cord[1]] + vec_2
        diff_2 = flush_edge_start.distanceToPoint(point_2)
        
        sections.append({
            "start": diff_1,
            "end": diff_2,
            "type": "window"
            })

    # TODO: Possible bug, producing far too many plates than needed

    # Create top and bottom plates
    bottom_plates = det_plate.make_bottom_plates(
            frame_space, frame_sides["bot"], frame_sides["plumb_vec"],
            frame_base_cardinal, plate_thickness
            )
    top_plates = det_plate.make_top_plates(
            frame_space, frame_sides["top"], frame_sides["plumb_vec"],
            frame_base_cardinal, plate_thickness
            )

    # Remove any intersection between bottom and top plates (from the top
    # plates)
    trimmed_top_plates = []
    for tp in top_plates:
        for bp in bottom_plates:
            tp = tp.cut(bp)
            trimmed_top_plates.append(tp)
    top_plates = trimmed_top_plates

    # Create the plates as objects within the document
    all_plates = bottom_plates + top_plates

    for plate in top_plates:
        top_plate_object = FreeCAD.ActiveDocument.addObject(
                "Part::Feature", "top_plate")
        top_plate_object.Shape = plate

    for plate in bottom_plates:
        bottom_plate_object = FreeCAD.ActiveDocument.addObject(
                "Part::Feature", "bottom_plate")
        bottom_plate_object.Shape = plate

    # Define stud space
    stud_space = frame_space
    for plate in all_plates:
        stud_space = stud_space.cut(plate)

    # Define the variables of the studs
    stud_width = 45
    stud_depth = 75
    stud_spacing = 400

    # Make studs
    det_stud.make_studs(
            stud_space, flush_face, frame_base_cardinal,
            stud_width, stud_depth, stud_spacing,
            sections
            )

    # Hide original object
    Gui.activeDocument().getObject(sel_object.Name).Visibility = False

    FreeCAD.ActiveDocument.commitTransaction()
