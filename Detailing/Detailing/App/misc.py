import os
import types
import importlib

import Detailing.App

def reload_code():

    reload_module = Detailing.App
    
    fn = reload_module.__file__
    fn_dir = os.path.dirname(fn) + os.sep
    module_visit = {fn}
    del fn

    def reload_recursive_ex(module):
        importlib.reload(module)

        for module_child in vars(module).values():
            if isinstance(module_child, types.ModuleType):
                fn_child = getattr(module_child, "__file__", None)
                if (fn_child is not None) and fn_child.startswith(fn_dir):
                    if fn_child not in module_visit:
                        print("Reloading: {}, from: {}".format(
                            fn_child, module
                            ))
                        module_visit.add(fn_child)
                        reload_recursive_ex(module_child)

    print()
    return reload_recursive_ex(reload_module)
