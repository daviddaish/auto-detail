import math
import itertools

import Part
import DraftVecUtils
import DraftGeomUtils

# Creates a cuboid by drawing between two cardinal direction directionaries
# (see the returned value of find_cardinal())
def make_cuboid(orig_vectors, extend_vectors):

    # Create sides
    n_side = make_side("n", orig_vectors, extend_vectors)
    w_side = make_side("w", orig_vectors, extend_vectors)
    e_side = make_side("e", orig_vectors, extend_vectors)
    s_side = make_side("s", orig_vectors, extend_vectors)

    # Find lowest and highest edges
    sides = [n_side,
             w_side,
             e_side,
             s_side]

    for s in sides:
        if len(s.Edges) != 4:
            raise ValueError(("While creating this cuboid, one of the sides had "
                    "{} edges, instead of the expected number, "
                    "4.").format(len(s.Edges)))

    lowest_edges = [s.Edges[1] for s in sides]
    highest_edges = [s.Edges[3] for s in sides]

    # Create top and bottom of cuboid
    face_top = Part.Face(Part.Wire(lowest_edges))
    face_bottom = Part.Face(Part.Wire(highest_edges))

    # Create cuboid, using each part
    cuboid_faces = sides + [face_top, face_bottom]
    cuboid_shell = Part.Shell(cuboid_faces)
    cuboid = Part.Solid(cuboid_shell)

    return cuboid

# Creates a side, starting at the point with the cardinal direction
# specified
def make_side(card, orig_vec, exte_vec):

    next_card = cardinal_order(card)
    side_wire = Part.makePolygon([
            orig_vec[card], exte_vec[card],
            exte_vec[next_card], orig_vec[next_card],
            orig_vec[card]])
    side_face = Part.Face(side_wire)
    return side_face

# Checks if the first shape is in the list of shapes
def find_shape(shape, shape_list):
    
    for s in shape_list:
        if shape.isSame(s):
            return True

    return False

# Checks if a face shares any vertexes with the list of vertexes.
def shared_vertexes(face, vertexes):
    
    face_vertexes = face.Vertexes

    for vertex in face_vertexes:
        if find_shape(vertex, vertexes):
            return True

    else: return False

def find_sides (space):

    bot_faces = find_bases(space)
    plumb_vec = bot_faces[0].normalAt(0, 0)
    plumb_vec = plumb_vec / plumb_vec.Length
    not_bottom_faces = [f for f in space.Faces if not find_shape(f, bot_faces)]

    downward_faces = [f for f in not_bottom_faces
            if f.normalAt(0,0) == plumb_vec]

    def check_right_ang_plumb(face):
        normal = face.normalAt(0,0)
        angle = DraftVecUtils.angle(normal, plumb_vec)

        if is_right_angle(angle):
            return True
        else:
            return False

    # If there are no openings (a window or door) a simple process can be used
    # to find the remaining sides.
    are_openings = len(downward_faces) != 0
    if not are_openings:

        side_faces = [f for f in not_bottom_faces if check_right_ang_plumb(f)]
        top_faces = [f for f in not_bottom_faces if f not in side_faces]

        return {
            "plumb_vec": plumb_vec,
            "bot": bot_faces,
            "side": side_faces,
            "inter": [],
            "top": top_faces
            }

    # If there are openings, a more complex process needs to be used to
    # correctly find the sides.
    else:

        sd_nb_f = []
        for df in downward_faces:
            for nbf in not_bottom_faces:
                if shared_vertexes(df, nbf.Vertexes):
                    sd_nb_f.append(nbf)


        end_faces = [f for f in not_bottom_faces 
                if f not in sd_nb_f and check_right_ang_plumb(f)]

        not_end_faces = [f for f in not_bottom_faces if f not in end_faces]

        length_faces = []
        for f in not_end_faces:
            if check_right_ang_plumb(f):
                for ef in end_faces:
                    if shared_vertexes(f, ef.Vertexes):
                        length_faces.append(f)
                        break



        side_faces = length_faces + end_faces

        not_side_faces = [f for f in not_bottom_faces if f not in side_faces]
        
        internal_faces = []
        for f in not_side_faces:
            for df in downward_faces:
                if shared_vertexes(f, df.Vertexes):
                    internal_faces.append(f)
                    break

        remaining_faces = [f for f in not_side_faces if f not in internal_faces]

        for f in remaining_faces:
            for inf in internal_faces:
                if shared_vertexes(f, inf.Vertexes):
                    internal_faces.append(f)
                    break

        top_faces = [f for f in remaining_faces if f not in internal_faces]

        return {
            "plumb_vec": plumb_vec,
            "bot": bot_faces,
            "side": side_faces,
            "inter": internal_faces,
            "top": top_faces
            }

# Given a list of faces, returns a list of faces grouped together by which are
# connected.
def group_by_contact(faces):

    if len(faces) == 0: return []

    groups = []

    start_face = faces[0]
    del faces[0]
    current_group = [start_face]

    while True:

        current_group_vertexes = list(itertools.chain.from_iterable(
            [v.Vertexes for v in current_group]
            ))

        found_connected_face = False

        for index, face in enumerate(faces):
            if shared_vertexes(face, current_group_vertexes):
                current_group.append(face)
                del faces[index]
                found_connected_face = True
                break

        if not found_connected_face:
            groups.append(current_group)

            if len(faces) == 0: break

            current_group = [faces[0]]
            del faces[0]

    return groups

# Returns the type of opening the faces are
# TODO: Make this a more rigourious test.
def type_of_opening(faces):
    if len(faces) == 3:
        return "ingress"

    elif len(faces) == 4:
        return "window"

    else:
        raise ValueError("Cannot determine the type of opening given.")

# Checks if the angle given is a right angle in radians 
def is_right_angle (angle): 

    given_angle = round(abs(angle), 10)
    right_angle = round((math.pi / 2), 10)
    return given_angle == right_angle

# Finds the bottom faces of a shape. Useful when a shape's base is split by an
# opening for a door or entryway.
def find_bases(shape):

    faces = shape.Faces
    lowest_face = find_lowest(faces)
    downwards_faces = [f for f in faces if f.normalAt(0,0) == lowest_face.normalAt(0,0)]
    base_faces = [f for f in faces if DraftGeomUtils.isCoplanar([f, lowest_face])]

    return (base_faces)

# Out of a list of Part.Shapes, determines which has the lowest total z value,
# as determined from their vertexes
def find_lowest(shape_list):
    z_value_avgs = []
    for shape in shape_list:
        z_values = [v.Z for v in shape.Vertexes]
        z_value_avgs.append(sum(z_values) / len(z_values))

    lowest_z_avg = min(z_value_avgs)
    return shape_list[z_value_avgs.index(lowest_z_avg)]

# Out of a list of Part.Shapes, determines which has the highest total z value,
# as determined from their vertexes
def find_highest(shape_list):
    z_value_avgs = []
    for shape in shape_list:
        z_values = [v.Z for v in shape.Vertexes]
        z_value_avgs.append(sum(z_values) / len(z_values))

    highest_z_avg = max(z_value_avgs)
    return shape_list[z_value_avgs.index(highest_z_avg)]


# Finds the "cardinal directions" of the corners of a rectangular face, in
# respect to their positions with each other (north, west, east, and south).

# If the second argument, orient_cords is given, it will base the cardinal
# directions off their similarity to the orientation co-ordinates instead of
# determining it from scratch. This leads to a more consistnt definition of
# N,W,E,S across a shape.

def find_cardinal(faces, orient_cords=False):

    # Continue if faces is just a single face.
    if isinstance(faces, Part.Face):

        main_face = faces
        if not check_is_rectangle(main_face):
            raise ValueError(("find_cardinal() only works with rectangle faces. "
                "It was passed a non-rectangular face."))

    # If it's a list, extract a single main face.
    elif isinstance(faces, list):
        vertexes = list(itertools.chain.from_iterable(
            [v.Vertexes for v in faces]
            ))
        points = [v.Point for v in vertexes]

        x_sorted = sorted(points, key=lambda p: p.x)
        most_x = x_sorted[:2]
        least_x = x_sorted[-2:]

        # Check if the algorithm has correctly found a rectangular shape.
        test_face = Part.Face(Part.makePolygon([
            max(most_x, key=lambda p: p.y),
            max(least_x, key=lambda p: p.y),
            min(most_x, key=lambda p: p.y),
            min(least_x, key=lambda p: p.y),
            max(most_x, key=lambda p: p.y)
            ]))

        if check_is_rectangle(test_face):
            main_face = test_face

        else:
            # If it hasn't reverse the order of sorting to Y, then X.
            y_sorted = sorted(points, key=lambda p: p.y)
            most_y = y_sorted[:2]
            least_y = y_sorted[-2:]

            test_face = Part.Face(Part.makePolygon([
                max(most_y, key=lambda p: p.x),
                min(most_y, key=lambda p: p.x),
                max(least_y, key=lambda p: p.x),
                min(least_y, key=lambda p: p.x),
                max(most_y, key=lambda p: p.x)
                ]))

            if not check_is_rectangle(test_face):
                raise ValueError(("Could not create a rectangular set of cardinal "
                    "points from the given faces."))

            else:
                main_face = test_face
        
    # Throw error if it isn't face, or a list
    else: 
        raise ValueError("find_cardinal() was not passed a face, or a list of faces.")

    points = [v.Point for v in main_face.Vertexes]

    if not orient_cords:

        x_sorted = sorted(points, key=lambda p: p.x)
        most_x = x_sorted[0:2]
        least_x = x_sorted[2:4]


        cardinal_points = {
                "n": max(most_x, key=lambda p: p.y),
                "w": max(least_x, key=lambda p: p.y),
                "e": min(most_x, key=lambda p: p.y),
                "s": min(least_x, key=lambda p: p.y)
            }

        return cardinal_points

    else:
        # Find the vectors for representing a NW and NE direction
        nw_vec = orient_cords["w"] - orient_cords["n"]
        nw_vec = nw_vec / nw_vec.Length

        ne_vec = orient_cords["e"] - orient_cords["n"]
        ne_vec = ne_vec / ne_vec.Length

        # Drawing vectors between an arbitrary origin point and the other
        # points, determine which vectors are most similiar to the NW and NE
        # directions. From that, determine the cardinal directions.

        #get multiple for each that match the magnitude

        nw_mag = 10
        nw_idx = []

        ne_mag = 10
        ne_idx = []
        
        rnd_prec = 5
        usable = lambda a: round(abs(a), rnd_prec)

        for orig_index, origin_p in enumerate(points):

            temp_points = points.copy()
            del temp_points[orig_index]

            for i, p in enumerate(temp_points):
                vec = p - origin_p
                vec = vec / vec.Length

                nw_sim = DraftVecUtils.angle(vec, nw_vec)
                ne_sim = DraftVecUtils.angle(vec, ne_vec)

                # Checking for similarities to NW direction
                if usable(nw_sim) == usable(nw_mag):
                    second_index = points.index(p)
                    nw_idx.append((orig_index, second_index))

                elif usable(nw_sim) < usable(nw_mag):
                    second_index = points.index(p)
                    
                    nw_mag = nw_sim
                    nw_idx = [(orig_index, second_index)]

                # Checking for similarities to NE direction
                if usable(ne_sim) == usable(ne_mag):
                    second_index = points.index(p)
                    ne_idx.append((orig_index, second_index))

                elif usable(ne_sim) < usable(ne_mag):
                    second_index = points.index(p)

                    ne_mag = ne_sim
                    ne_idx = [(orig_index, second_index)]

        if len(nw_idx) != 2 or len(ne_idx) != 2:
            raise ValueError(("Something went wrong when determining the"
                              "correlation between points."))

        # We can determine which index is the north coordinate, because the
        # northern index will appear as the first item in _two_ of the touples.
        # Similarly, the southern index will appear as the second item in two
        # of the touples.
        all_idx = nw_idx + ne_idx
        first_items = [i[0] for i in all_idx]
        north_index = max(set(first_items), key=first_items.count)

        second_items = [i[1] for i in all_idx]
        south_index = max(set(second_items), key=second_items.count)

        west_index = [t for t in nw_idx if t[0] == north_index][0][1]
        east_index = [t for t in ne_idx if t[0] == north_index][0][1]

        cardinal_points = {
                "n": points[north_index],
                "w": points[west_index],
                "e": points[east_index],
                "s": points[south_index]
            }

        return cardinal_points

# Returns the next cardinal direction, in a clockwise direction.
def cardinal_order (cardinal_string):
    if cardinal_string == "n": return "e"
    elif cardinal_string == "e": return "s"
    elif cardinal_string == "s": return "w"
    elif cardinal_string == "w": return "n"
    else: raise ValueError("Not a known cardinal direction.")

# Given a touple representing a direction based on cardinal directions, returns
# a touple representing the paralell direction.
def cardinal_parallel (edge_touple):
    edge_set = set(edge_touple)

    if edge_set == set(["n", "e"]):
        # Therefore, opposite cardinals are sw

        if edge_touple[0] is "n":
            opposite_touple = ("w", "s")
        else:
            opposite_touple = ("s","w")

    elif edge_set == set(["n", "w"]):
        # Therefore, opposite cardinals are se

        if edge_touple[0] is "n":
            opposite_touple = ("e", "s")
        else:
            opposite_touple = ("s","e")

    elif edge_set == set(["s", "e"]):
        # Therefore, opposite cardinals are nw

        if edge_touple[0] is "s":
            opposite_touple = ("w", "n")
        else:
            opposite_touple = ("n","w")

    elif edge_set == set(["s", "w"]):
        # Therefore, opposite cardinals are ne

        if edge_touple[0] is "s":
            opposite_touple = ("e", "n")
        else:
            opposite_touple = ("n","e")

    else:
        raise ValueError("Unrecognised cardinal direction.")

    return opposite_touple

# When given a Part.Face object, ensures it is a rectangle.
def check_is_rectangle(face):

    points = [v.Point for v in face.Vertexes]

    # Filter out faces with more than 4 edges
    if len(points) is not 4:
        return False

    # Filter out faces that do not have right angles with adjacent edges
    for point in points:
        other_points = [p for p in points if p is not point]

        # Get the vectors between this point and the other points
        vectors = []
        for other_point in other_points:
            vectors.append(point - other_point)

        # Get the right angles between these vectors
        angles = [
            DraftVecUtils.angle(vectors[0], vectors[1]),
            DraftVecUtils.angle(vectors[0], vectors[2]),
            DraftVecUtils.angle(vectors[1], vectors[2])
        ]
        angles = [math.degrees(a) for a in angles]
        right_angles = [a for a in angles if round(abs(a), 5) == 90]

        if len(right_angles) == 0:
            return False

    return True

