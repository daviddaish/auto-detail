import math

import FreeCAD
import Part

import Detailing.App.common as det_common

def make_dwangs(stud_spaces, flush_face, width, depth_length, depth_vector, stagger):

    # How short does a stud have to be before it doesn't need a dwang:
    no_dwang_height = 600

    dwang_depth_vector = depth_vector * depth_length

    previous_dwang_heights = []

    # Iterate over dwang spaces
    for i in range(len(stud_spaces) - 1):

        # Find longest travel edge
        travel_edge_0 = Part.LineSegment(
                stud_spaces[i+1]["bot"][flush_face[0]],
                stud_spaces[i+1]["top"][flush_face[0]]
                ).toShape().Edges[0]

        travel_edge_1 = Part.LineSegment(
                stud_spaces[i]["bot"][flush_face[1]],
                stud_spaces[i]["top"][flush_face[1]]
                ).toShape().Edges[0]

        if travel_edge_0.Length < travel_edge_1.Length:
            travel_edge = travel_edge_0
            dwang_length_vector = (
                    stud_spaces[i]["bot"][flush_face[1]] -
                    stud_spaces[i+1]["bot"][flush_face[0]]
                    )
        else:
            travel_edge = travel_edge_1
            dwang_length_vector = (
                    stud_spaces[i+1]["bot"][flush_face[0]] -
                    stud_spaces[i]["bot"][flush_face[1]]
                    )

        if travel_edge.Length > no_dwang_height:
            
            previous_dwang_heights = make_dwang(
                    travel_edge, flush_face, dwang_depth_vector, width,
                    dwang_length_vector, stagger, previous_dwang_heights
                    )

def make_dwang (travel_edge, flush_face, depth_vector, width_length,
        length_vector, stagger, previous_placements):

    # Determine how many dwangs to use
    dwang_num = math.ceil(travel_edge.Length / 1350)
    space_num = dwang_num + 1
    spacing = travel_edge.Length / space_num

    paralell_face = det_common.cardinal_parallel(flush_face)

    placements = []

    for i in range(dwang_num):

        # Determine where to place dwang
        placement = (spacing - (width_length / 2)) * (i+1)

        # If the placement is too close to a previous one, stagger it
        for place in previous_placements:
            if placement > (place - stagger/2) and placement < (place + stagger/2):
                placement = placement - stagger

        if placement < 0 or placement + width_length > travel_edge.Length:
            raise ValueError(("The dwang stagger algorithm has moved the dwang "
                    "to an impossible location."))

        placements.append(placement)

        # Determine dwang co-ordinates
        bot_dwang_cords = {}
        top_dwang_cords = {}

        bot_dwang_cords[flush_face[0]] = travel_edge.valueAt(placement)
        bot_dwang_cords[flush_face[1]] = bot_dwang_cords[flush_face[0]] + length_vector
        bot_dwang_cords[paralell_face[0]] = bot_dwang_cords[flush_face[0]] + depth_vector
        bot_dwang_cords[paralell_face[1]] = bot_dwang_cords[flush_face[1]] + depth_vector

        top_dwang_cords[flush_face[0]] = travel_edge.valueAt(placement + width_length)
        top_dwang_cords[flush_face[1]] = top_dwang_cords[flush_face[0]] + length_vector
        top_dwang_cords[paralell_face[0]] = top_dwang_cords[flush_face[0]] + depth_vector
        top_dwang_cords[paralell_face[1]] = top_dwang_cords[flush_face[1]] + depth_vector

        # Create dwang
        dwang = det_common.make_cuboid(bot_dwang_cords, top_dwang_cords)
        dwang_object = FreeCAD.ActiveDocument.addObject("Part::Feature", "dwang")
        dwang_object.Shape = dwang

    return placements

