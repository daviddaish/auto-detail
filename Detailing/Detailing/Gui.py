import os

import FreeCAD
if FreeCAD.GuiUp:
	import FreeCADGui as Gui

import Detailing.App.misc
import Detailing.App.frame

class command_frame:
    def GetResources(self):
        return {
            "MenuText": "Frame",
            "Accel": "f,r",
            "ToolTip": "Converts a cuboid into a timber frame.",
            "Pixmap"  : os.path.join(os.path.dirname(__file__), "icons", "frame.svg")
            }

    def Activated(self):
        sel = Gui.Selection.getSelection()
        if not sel:
            FreeCAD.Console.PrintMessage("Nothing is selected.")
        else:
            Detailing.App.frame.make_frame(sel[0])

Gui.addCommand('detailing_frame', command_frame())


class command_reload_code:
    def GetResources(self):
        return {
            "MenuText": "Reload code",
            "Accel": "r,r",
            "ToolTip": "Reload the code of the Detailing module",
            "Pixmap"  : os.path.join(os.path.dirname(__file__), "icons", "code_reload.svg")
            }

    def Activated(self):
        Detailing.App.misc.reload_code()

Gui.addCommand('reload_code', command_reload_code())

