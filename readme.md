# Auto-detail

In the New Zealand frame and truss manufacturing industry, detailing is the
process of converting a client’s floor plan into a layout that can be used by
frame and truss machinery.

# Setup

This is a note to myself that only makes sense on my workstation.

```
/mnt/build/bin/FreeCAD --module-path /mnt/files/code/auto-detail/Detailing/ --log-file logfile
```

## Todo

* Add door and window openings
  * Add a system for determining openings, ingresses, and regular stretches.
* Add calculations of appropriate member sizes
* Add bracing
* Add tests
* Add detecting real world wall dimensions
* Add user interface for making tweaks
* Add exporting data
